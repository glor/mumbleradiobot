### Dependencies:
- `node`
- `npm` or `yarn` if you need/want to update the dependencies

### Update Dependencies
You should probably do `npm install .` or `yarn` (in the repos directory). If there are errors, it might stil work.

### How to run:
`node index.js`

### How to change:
Just change the url in `index.js`